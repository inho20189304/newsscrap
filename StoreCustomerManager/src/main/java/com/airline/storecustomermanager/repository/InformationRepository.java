package com.airline.storecustomermanager.repository;

import com.airline.storecustomermanager.entity.Information;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformationRepository extends JpaRepository<Information, Long> {
}
