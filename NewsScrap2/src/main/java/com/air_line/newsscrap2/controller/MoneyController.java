package com.air_line.newsscrap2.controller;


import com.air_line.newsscrap2.model.MoneyChangeRequest;
import com.air_line.newsscrap2.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
    @RequestMapping("/money")
@RequiredArgsConstructor
public class MoneyController {
    private final MoneyService moneyService;

    @PostMapping("/chagne")
    public String peopleChange(@RequestBody MoneyChangeRequest  request) {
        String result = moneyService.convertMoney(request.getMoney());
        return result;
    }

    @GetMapping("/pay-back")
    public String peoplePayBack(){
        return "환불되었습니다. 고객님";
    }
}
