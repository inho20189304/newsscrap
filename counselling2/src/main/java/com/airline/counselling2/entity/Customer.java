package com.airline.counselling2.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.swing.*;

@Entity
@Getter
@Setter
public class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 20)
    private String phone;

}
