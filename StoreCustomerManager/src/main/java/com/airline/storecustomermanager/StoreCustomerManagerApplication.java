package com.airline.storecustomermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreCustomerManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreCustomerManagerApplication.class, args);
    }

}
