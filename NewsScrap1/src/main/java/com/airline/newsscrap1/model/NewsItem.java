package com.airline.newsscrap1.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScrapModel {

    private String title;
    private String content;
}
