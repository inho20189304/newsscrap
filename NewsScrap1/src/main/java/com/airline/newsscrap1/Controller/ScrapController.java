package com.airline.newsscrap1.Controller;

import com.airline.newsscrap1.Service.ScrapService;
import com.airline.newsscrap1.model.NewsItem;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class TempController {

    private final ScrapService scrapService;

    @GetMapping("/Html")
    public List<NewsItem> getHtml() {
        List<NewsItem> result = scrapService.run();
        return result;
    }
}
