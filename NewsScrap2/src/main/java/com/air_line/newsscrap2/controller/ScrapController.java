package com.air_line.newsscrap2.controller;

import com.air_line.newsscrap2.model.NewsItem;
import com.air_line.newsscrap2.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService;

    @GetMapping("/Html")
    public List<NewsItem> getHtml() throws IOException {
        return scrapService.run();

    }
}
