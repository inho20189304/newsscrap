package com.example.controllerpractice.controller;

import com.example.controllerpractice.model.MoneyChangeRequest;
import com.example.controllerpractice.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.support.SimpleTriggerContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/money")
@RequiredArgsConstructor
public class MoneyController {

    private final MoneyService moneyService;

    @PostMapping("/change")
    public String peoplechange(@RequestBody MoneyChangeRequest request) {

        String result  = moneyService.convertMoney(request.getMoney());
        return result;
    }
    private final   MoneyService moneyService

    @GetMapping("/refund")
    public String peoplerefund(@RequestBody MoneyChangeRequest request2) {
        String result2 = MoneyService(request2.getRefundmoney());
        return result2;
    }




}
