package com.airline.counselling2.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class CustomerModel {
    private String name;
    private String phone;

}
