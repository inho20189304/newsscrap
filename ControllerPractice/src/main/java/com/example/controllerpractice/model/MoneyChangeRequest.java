package com.example.controllerpractice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyChangeRequest {
    private Integer money;

    private Integer refundmoney;
}
