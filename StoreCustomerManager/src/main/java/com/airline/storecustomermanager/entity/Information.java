package com.airline.storecustomermanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Information {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false, length = 20)
    private String buy;

    @Column(nullable = false)
    private Integer age;



}
