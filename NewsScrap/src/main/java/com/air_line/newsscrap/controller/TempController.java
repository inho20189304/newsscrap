package com.air_line.newsscrap.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/money")
public class TempController {

    @GetMapping("/change")
    public String peopleChange() {
        return "교환되었습니다. 고객님";
    }
    @GetMapping("/pay-back")
    public String peoplePayback() {
        return "환불되었습니다. 고객님";
    }
}
