package com.air_line.newsscrap2.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewsItem {
    private String title;
    private String content;

}
